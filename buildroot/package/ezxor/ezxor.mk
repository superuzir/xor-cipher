################################################################################
#
# ezxor
# ################################################################################

EZXOR_VERSION = 1.0.0
EZXOR_SITE_METHOD = local
EZXOR_SITE = /home/user/git/srconly
EZXOR_LICENSE = GPL-2.0
EZXOR_LICENSE_FILES = COPYING

$(eval $(kernel-module))
$(eval $(generic-package))
