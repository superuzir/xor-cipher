#include <linux/module.h>
#include <linux/crypto.h>

#define NAME "ezxor"


static void crypt(struct crypto_tfm *tfm, u8 *dst, const u8 *src){
    *dst = *src ^ 42;
}


static int setkey(struct crypto_tfm *tfm, const u8 *key, unsigned int key_len){
    return 0;
}


static struct crypto_alg alg = {
    .cra_name        = NAME,
    .cra_driver_name = NAME "-generic",
    .cra_priority    = 100,
    .cra_flags       = CRYPTO_ALG_TYPE_CIPHER,
    .cra_blocksize   = 1,
    .cra_module      = THIS_MODULE,
    .cra_u           = {
        .cipher = {
            .cia_min_keysize = 8,
            .cia_max_keysize = 256,
            .cia_setkey      = setkey,
            .cia_encrypt     = crypt,
            .cia_decrypt     = crypt
        }
    }
};


static int __init init(void){
    return crypto_register_alg(&alg);
}


static void __exit deinit(void){
    crypto_unregister_alg(&alg);
}


module_init(init);
module_exit(deinit);

MODULE_LICENSE("GPL");
MODULE_ALIAS_CRYPTO(NAME);
