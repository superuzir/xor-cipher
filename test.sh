#!/bin/sh -ex

mkdir -p test
cd test

IMG=binary.img
CIPHER=ezxor-ecb
DMNAME=xortest

PLAINTEXT="abcdefghi"
CIPHERTEXT="KHINOLMBC"

dd if=/dev/zero bs=1M count=4 > $IMG
mkdir -p mnt
echo qwerty > pass.txt

# Create encrypted partition with known plaintext
cryptsetup luksFormat --batch-mode -c $CIPHER $IMG --key-file=pass.txt

cryptsetup luksOpen $IMG $DMNAME --key-file=pass.txt

mkdosfs /dev/mapper/$DMNAME

mount /dev/mapper/$DMNAME mnt

echo $PLAINTEXT | tee mnt/secret.txt

umount /dev/mapper/$DMNAME

cryptsetup luksClose $DMNAME

# Check that plaintext is not found in the underlying file
! grep -q $PLAINTEXT $IMG

# Check that XORed plaintext is present somewhere in the underlying binary
grep -q $CIPHERTEXT $IMG

# Check that we can decrypt the volume
cryptsetup luksOpen $IMG $DMNAME --key-file=pass.txt

mount /dev/mapper/$DMNAME mnt

grep -q $PLAINTEXT mnt/secret.txt

umount /dev/mapper/$DMNAME

cryptsetup luksClose $DMNAME

# If we got here then none of the above commands failed 
# because script is run with -e
echo OK
