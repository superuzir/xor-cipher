## XOR cipher

This project contains example kernel module for Linux crypto subsystem. Crypto function is trivial and similar for both encryption and decryption:

`Ci = Pi XOR 0x2A`


### Installation
Compile

`make`


To insert module into the kernel 

`sudo insmod ezxor.ko`


### Tests

Tests perform the following operations

#### Create encrypted partition

1. Create a file which will be accessed as a block device
2. Create a LUKS partition over this file with ezxor-ecb driver specified as a cipher
3. Map this partition to another block device with device mapper and mount it
4. Create a text file with string 'abcdefghi'
5. Unmount, luksClose

#### Check encryption

1. Since plain text is known and key is a constant (42) we may calculate XORed string ('abcdefghi' XOR 42 = 'KHINOLMBC')
2. It is expected that this ciphertext should be found in the underlying binary file
3. On the other hand plain text should not be found

#### Check decryption

1. LUKSopen and mount partition
2. Check that test file contains initital string


To run tests:

`./test.sh`
